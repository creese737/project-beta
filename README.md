# CarCar

Team:

* Cierra Reese - Service microservice?
* Jonas Tengesdal - Sales microservice

## Design
CarCar is a webapp where a dealership can manage their inventory, sales, and services. 
It has React frontend, and a Django backend that deals with the inventory, sales, and services microservices. 

## Setup Steps
In your terminal => git clone https://gitlab.com/jonastengesdal/project-beta.git
Switch to this projects directory => cd project-beta
Open Visual studio code with => code .
Create docker volume => docker volume create beta-data
Create docker containers => docker-compose build
Run containers => docker-compose up
Go to http://localhost:3000/ to access the React frontend in your browser

## Inventory microservice

Manufacturers
Create method == POST with url http://localhost:8100/manufacturers/
List all method == GET with url http://localhost:8100/manufacturers/
Get specific manufacturer details method == GET with url  http://localhost:8100/manufacturers/:id/
Update specific manufacturer method == PUT with url http://localhost:8100/manufacturers/:id/
Delete method == DELETE with url http://localhost:8100/manufacturers/:id/

Models
Create method == POST with url http://localhost:8100/models/
List all method == GET with url http://localhost:8100/models/
Get specific model details method == GET with url  http://localhost:8100/models/:id/
Update specific model method == PUT with url http://localhost:8100/models/:id/
Delete method == DELETE with url http://localhost:8100/models/:id/

Automobiles
Create method == POST with url http://localhost:8100/automobiles/
List all method == GET with url http://localhost:8100/automobiles/
Get specific automobile details method == GET with url  http://localhost:8100/automobiles/:id/
Update specific automobile method == PUT with url http://localhost:8100/automobiles/:id/
Delete method == DELETE with url http://localhost:8100/automobiles/:id/

## Entering data for Inventory
POST request to http://localhost:8100/manufacturers/
{
    "name": Chrysler
}

POST request to http://localhost:8100/models/
{
    "name": Toyota
    "picture_url": 
    "manufacurer"
}

POST request to http://localhost:8100/automobiles/
{
    "color":
    "year":
    "vin":
    "model_id":
}

## Service microservice

The service microservice that integrates with the inventory microservice by polling for automobile vin data and creating value objects to handle the data. The microservice backend uses the models AutomobileVO, Technician, and Appointment. There were issues with trying to implement AppointmentList and ServiceHistory on the frontend

Technicians
Create method == POST with url http://localhost:8100/technicians/
List all method == GET with url http://localhost:8100/technicians/
Get specific automobile details method == GET with url  http://localhost:8100/technicians/:id/
Update specific automobile method == PUT with url http://localhost:8100/technicians/:id/
Delete method == DELETE with url http://localhost:8100/technicians/:id/

Appointments
Create method == POST with url http://localhost:8100/appointments/
List all method == GET with url http://localhost:8100/appointments/
Get specific automobile details method == GET with url  http://localhost:8100/appointments/:id/
Update specific automobile method == PUT with url http://localhost:8100/appointmentss/:id/
Delete method == DELETE with url http://localhost:8100/appointments/:id/

## Entering data for Service
POST request to http://localhost:8100/technicians/
{
    "name": Tony,
    "employee_num": 1234
}
POST request to http://localhost:8080/api/appointments/
{
    "customer": "Minari",
    "date": "2023-01-30",
	"time": "09:00:00",
	"technician": 1,
	"vip": false,
	"reason_for_service": "Oil Change",
	"finished": false
}

## Sales microservice

## How to Run this Application

Insert steps to clone and run application
Through the terminal

## Applicaiton Diagram

Insert diagram of project here

## CRUD Routes, API Documentation
Record Service:
Localhost, Port 8090


GET request to /api/records/

Returns:
```
{
	"records": [
		{
			"automobile": {
				"vin": "12345678901234",
				"import_href": "",
				"year": 2006
			},
			"person": {
				"name": "Mike",
				"employee_number": 1,
				"id": 1
			},
			"customer": {
				"name": "Dan",
				"address": "12345 1st st se",
				"phone_number": "123-123-123",
				"id": 2
			},
			"price": 123,
			"id": 1
		},
```

POST request to /api/records/

Request body:
```
{
	"automobile": "12345678901234",
	"customer": 2,
	"person": 1,
	"price": 1234
}
```

Returns (status code 200):
```
{
	"automobile": {
		"vin": "12345678901234",
		"import_href": "",
		"year": 2006
	},
	"person": {
		"name": "Mike",
		"employee_number": 1,
		"id": 1
	},
	"customer": {
		"name": "Dan",
		"address": "12345 1st st se",
		"phone_number": "123-123-123",
		"id": 2
	},
	"price": 4567,
	"id": 4
}
```

Customer Service:
Localhost, Port 8090


GET request to /api/customers/

Returns:
```
{
	"customers": [
		{
			"name": "Dan",
			"address": "123 1st st se",
			"phone_number": "123-123-123",
			"id": 2
		}
	]
}
```

POST request to /api/records/

Request body:
```
{
	"name": "Dan",
	"address": "123 1st st se",
	"phone_number": "123-123-123",
	"id": 2
}
```

Returns (status code 200):
```
{
	"name": "Dan",
	"address": "123 1st st se",
	"phone_number": "123-123-123",
	"id": 2
}
```

Sales People Service:
Localhost, Port 8090


GET request to /api/people/

Returns:
```
{
	"people": [
		{
			"name": "Mike",
			"employee_number": 1,
			"id": 1
		}
	]
}
```

POST request to /api/records/

Request body:
```
{
	"name": "Erin",
	"employee_number": "2",
	"id": 2
}
```

Returns (status code 200):
```
{
	"name": "Erin",
	"employee_number": "2",
	"id": 2
}
```


Automobile Service:
Localhost, Port 8090


GET request to /api/automobiles//

Returns:
```
{
	"autos": [
		{
			"href": "/api/automobiles/12345678901234/",
			"id": 1,
			"color": "Maroon",
			"year": 2006,
			"vin": "12345678901234",
			"model": {
				"href": "/api/models/1/",
				"id": 1,
				"name": "Civic",
				"picture_url": "https://www.carscoops.com/wp-content/uploads/2022/07/1997-Honda-Civic-Type-R.jpg",
				"manufacturer": {
					"href": "/api/manufacturers/1/",
					"id": 1,
					"name": "Honda"
				}
			}
		}
	]
}
```

POST request to /api/records/

Request body:
```
{
	"color": "Maroon",
	"year": 2006,
	"vin": "12345678901234",
	"model_id": 1
}
```

Returns (status code 200):
```
{
	"href": "/api/automobiles/12345678901234/",
	"id": 1,
	"color": "Maroon",
	"year": 2006,
	"vin": "12345678901234",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "Civic",
		"picture_url": "https://www.carscoops.com/wp-content/uploads/2022/07/1997-Honda-Civic-Type-R.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Honda"
		}
	}
}



Vehicle Model Service:
Localhost, Port 8090


GET request to /api/models/

Returns:
```
{
	"models": [
		{
			"href": "/api/models/1/",
			"id": 1,
			"name": "Civic",
			"picture_url": "https://www.carscoops.com/wp-content/uploads/2022/07/1997-Honda-Civic-Type-R.jpg",
			"manufacturer": {
				"href": "/api/manufacturers/1/",
				"id": 1,
				"name": "Honda"
			}
		}
	]
}
```

POST request to /api/records/

Request body:
```
{
	"name": "Civic",
	"picture_url": "https://www.carscoops.com/wp-content/uploads/2022/07/1997-Honda-Civic-Type-R.jpg",
	"manufacturer_id": 1
}

```

Returns (status code 200):
```
{
	"href": "/api/models/1/",
	"id": 1,
	"name": "Civic",
	"picture_url": "https://www.carscoops.com/wp-content/uploads/2022/07/1997-Honda-Civic-Type-R.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Honda"
	}
}



Manufacturer Service:
Localhost, Port 8090


GET request to /api/manufacturers//

Returns:
```
{
	"manufacturers": [
		{
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Honda"
		}
	]
}
```

POST request to /api/records/

Request body:
```
{
	"id": 1,
	"name": "Honda"
}
```

Returns (status code 200):
```
{
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "Honda"
}



## Design

## Service microservice

Explain your models and integration with the inventory
microservice, here.

## Sales microservice

The sales microservice integrates with the inventory microservice by polling for automobile vin data and creating value objects to handle the data.
The microservice backend uses the AutomobileVo, Record, Customer, and Person models.
There were issues implementing Recordlist.js and RecordHistory on the frontend.
