from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import AutomobileVO, Person, Customer, Record


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "import_href",
        "year",
    ]


class PersonEncoder(ModelEncoder):
    model = Person
    properties = [
        "name",
        "employee_number",
        "id",
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "name",
        "address",
        "phone_number",
        "id",
    ]

class RecordEncoder(ModelEncoder):
    model = Record
    properties = [
        "automobile",
        "person",
        "customer",
        "price",
        "id",
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "person": PersonEncoder(),
        "customer": CustomerEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_people(request):
    if request.method == "GET":
        people = Person.objects.all()
        return JsonResponse(
            {"people": people},
            encoder=PersonEncoder
        )

    else:
        try:
            content = json.loads(request.body)
            person = Person.objects.create(**content)
            return JsonResponse(
                person,
                encoder=PersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Invalid customer"},
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_people(request, pk):
    if request.method == "GET":
        try:
            person = Person.objects.get(id=pk)
            return JsonResponse(
                person,
                encoder=PersonEncoder,
                safe=False,
            )
        except Person.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Sales Person"},
                status=400,
            )
    elif request.method == "DELETE":
        count, _ = Person.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Person.objects.filter(id=pk).update(**content)
        person = Person.objects.get(id=pk)
        return JsonResponse(
            person,
            encoder=PersonEncoder,
            safe=False,
        )






@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder
        )

    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Invalid customer"},
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_customers(request, pk):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=pk)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Customer"},
                status=400,
            )
    elif request.method == "DELETE":
        count, _ = Customer.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Customer.objects.filter(id=pk).update(**content)
        customer = Customer.objects.get(id=pk)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )




@require_http_methods(["GET", "POST"])
def api_list_records(request):
    if request.method == "GET":
        records = Record.objects.all()
        return JsonResponse(
            {"records": records},
            encoder=RecordEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:

            automobile = AutomobileVO.objects.get(vin = content["automobile"])
            content["automobile"]= automobile
            person = Person.objects.get(id = content["person"])
            content["person"] = person
            customer = Customer.objects.get(id = content["customer"])
            content["customer"] = customer

        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile record"},
                status=400,
            )

        record = Record.objects.create(**content)
        return JsonResponse(
            record,
            encoder=RecordEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_records(request, pk):
    if request.method == "GET":
        try:
            record = Record.objects.get(id=pk)
            return JsonResponse(
                record,
                encoder=RecordEncoder,
                safe=False,
            )
        except record.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Record"},
                status=400,
            )
    elif request.method == "DELETE":
        count, _ = Record.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Record.objects.filter(id=pk).update(**content)
        record = Record.objects.get(id=pk)
        return JsonResponse(
            record,
            encoder=RecordEncoder,
            safe=False,
        )
