from django.urls import path
from .views import (
    api_list_people,
    api_show_people,

    api_list_customers,
    api_show_customers,

    api_list_records,
    api_show_records,
)

urlpatterns = [
    path("people/", api_list_people, name="api_list_people"),
    path("people/<int:pk>/", api_show_people, name="api_show_people"),

    path("customers/", api_list_customers, name="api_list_customers"),
    path("customers/<int:pk>/", api_show_customers, name="api_show_customers"),

    path("records/", api_list_records, name="api_list_records"),
    path("records/<int:pk>/", api_show_records, name="api_show_records"),
]
