from django.contrib import admin
from .models import Person, Customer, Record


admin.site.register(Person)
admin.site.register(Customer)
admin.site.register(Record)
