from django.db import models
# from phone_field import PhoneField



class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200)
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)


class Person(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.PositiveSmallIntegerField()


class Customer(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=13)


class Record(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="records",
        on_delete=models.CASCADE,
    )
    person= models.ForeignKey(
        Person,
        related_name="records",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="records",
        on_delete=models.CASCADE,
    )
    price = models.PositiveIntegerField()
