from django.urls import path
from .views import api_list_technicians, api_show_technician, api_list_appointment, api_show_appointment, api_service_history

urlpatterns = [
    path("technicians/", api_list_technicians, name="api_list_technicians"),
    path("technicians/<int:pk>/", api_show_technician, name="api_show_technician"),
    path("appointments/", api_list_appointment, name="api_list_appointment"),
    path("appointments/<int:pk>/", api_show_appointment, name="api_show_appointment"),
    path("vin/appointment/<str:vin>", api_service_history, name="api_service_history"),
]