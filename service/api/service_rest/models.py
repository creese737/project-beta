from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return f"{self.vin}"


class Technician(models.Model):
    name = models.CharField(max_length=100, unique=True)
    employee_num = models.PositiveIntegerField()

    def __str__(self): 
        return f"{self.name}"

    def get_api_url(self):
        return reverse("api_show_technician", kwargs={"pk": self.pk})


class Appointment(models.Model):
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=100)
    date = models.DateField(null=True)
    time = models.TimeField(null=True)
    technician = models.ForeignKey(
        Technician,
        related_name='appointments',
        on_delete=models.CASCADE,
    )
    vip = models.BooleanField(default=False)
    reason_for_service = models.CharField(max_length=255)
    finished = models.BooleanField(default=False)

    def get_api_url(self):
        return reverse("api_show_appointment", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.customer} - {self.date}"