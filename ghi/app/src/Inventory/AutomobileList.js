function AutomobileList(props){
    return(
        <table className="table table-striped">
            <thead>
                <tr>
                <th>Vin</th>
                <th>Color</th>
                <th>Year</th>
                <th>Model</th>
                </tr>
            </thead>
            <tbody>
            {props.automobiles.map(automobile => {
                return (
                <tr key={automobile.id}>
                    <td>{automobile.vin}</td>
                    <td>{automobile.color}</td>
                    <td>{automobile.year}</td>
                    <td>{automobile.model.name}</td>
                </tr>
                )
            })}
            </tbody>
    </table>
    );
}
export default AutomobileList;