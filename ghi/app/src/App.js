import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useEffect , useState } from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianForm from './services/TechnicianForm';
import TechnicianList from './services/TechnicianList';
import AppointmentForm from './services/AppointmentForm';
import AppointmentList from './services/AppointmentList';
import ServiceHistory from './services/ServiceHistory';
import AutomobileForm from './Inventory/AutomobileForm';
import AutomobileList from './Inventory/AutomobileList';
import ManufacturerForm from './Inventory/ManufacurerForm';
import ManufacturerList from './Inventory/ManufacturerList';
import VehicleModelForm from './Inventory/VehicleForm';
import VehicleModelList from './Inventory/VehicleList';
import SalesList from './SalesList';
import PeopleList from './PeopleList';
import PersonForm from './PersonForm';
import CustomersList from './CustomersList';
import CustomerForm from './CustomerForm';
import RecordsList from './RecordsList';
import RecordForm from './RecordForm';


function App() {
  const [technicians, setTechnicians] = useState([])
  const [appointments, setAppointments] = useState([])
  const [automobiles, setAutomobiles] = useState([])
  const [manufacturers, setManufacturers] = useState([])
  const [models, setVehicleModels] = useState([])
  const [sales, setSales] = useState([])
  const [people, setPeople] = useState([])
  const [customers, setCustomers] = useState([])
  const [records, setRecords] = useState([])


  const getTechnicians = async () => {
    const url = "http://localhost:8080/api/technicians/"
    const response = await fetch(url)
    if (response.ok){
      const data = await response.json();
      const technicians = data.technicians
      setTechnicians(technicians)
    }
  }
  const getAppointments = async () => {
    const url = "http://localhost:8080/api/appointments/"
    const response = await fetch(url)
    if (response.ok){
      const data = await response.json();
      setAppointments(appointments)
    }
  }
  const getAutomobiles = async () => {
    const url = 'http://localhost:8100/api/automobiles/'
    const response = await fetch(url)
    if (response.ok) {
      const data = await response.json()
      setAutomobiles(data.autos)
      }
  }
  const getManufacturers = async () => {
    const url = "http://localhost:8100/api/manufacturers/"
    const response = await fetch (url)
    if (response.ok){
        const data = await response.json();
        const manufacturers = data.manufacturers
        setManufacturers(manufacturers)
    }
  }
  const getVehicleModels = async () => {
    const url = "http://localhost:8100/api/models/"
    const response = await fetch (url)
    if (response.ok){
        const data = await response.json();
        setVehicleModels(data.models)
    }
  }
  const getSales = async () => {
    const url = "http://localhost:8090/api/sales/"
    const response = await fetch(url)
    if (response.ok){
      const data = await response.json();
      setSales(data.sales)
    }
  }
  const getPeople = async () => {
    const url = "http://localhost:8090/api/people/"
    const response = await fetch (url)
    if (response.ok){
        const data = await response.json();
        setPeople(data.people)
    }
  }
  const getCustomers = async () => {
    const url = "http://localhost:8090/api/customers/"
    const response = await fetch (url)
    if (response.ok){
        const data = await response.json();
        setCustomers(data.customers)
    }
  }
  const getRecords = async () => {
    const url = "http://localhost:8090/api/records/"
    const response = await fetch (url)
    if (response.ok){
        const data = await response.json();
        setRecords(data.records)
    }
  }


  useEffect(() => {
    getTechnicians();
    getAppointments();
    getAutomobiles();
    getManufacturers();
    getVehicleModels();
    getSales();
    getPeople();
    getCustomers();
    getRecords();
  }, [])

    return (
      <BrowserRouter>
        <Nav />
        <div className="container">
          <Routes>
            <Route path="/" element={<MainPage />} />
            <Route path="technicians">
            <Route path="" element={<TechnicianList technicians={technicians} getTechnicians={getTechnicians} />}/>
            <Route path="new" element={<TechnicianForm getTechnicians={getTechnicians}/>} />
            </Route>
          <Route path="appointments">
            <Route path="" element={<AppointmentList appointments={appointments} getAppointments={getAppointments} />}/>
            <Route path="new" element={<AppointmentForm getAppointments={getAppointments} technicians={technicians} />}/>
            <Route path="history"  element={<ServiceHistory appointments={appointments} />}/>
          </Route>
          <Route path="manufacturers">
            <Route index element={<ManufacturerList manufacturers={manufacturers} />} />
            <Route path="new" element={<ManufacturerForm getManufacturers={getManufacturers} />} />
          </Route>
          <Route path="automobiles">
            <Route path="" element={<AutomobileList automobiles={automobiles} getAutomobiles={getAutomobiles}/>} />
            <Route path="new" element={<AutomobileForm getAutomobiles={getAutomobiles} models={models}/>} />
          </Route>
          <Route path="models">
              <Route index element={<VehicleModelList models={models} />} />
              <Route path="new" element={<VehicleModelForm manufacturers={manufacturers} getVehicleModels={getVehicleModels} />} />
          </Route>
          <Route path="sales">
              <Route path="" element={<SalesList sales={sales} getSales={getSales} />} />
              <Route path="new" element={<RecordForm customers={customers} getCustomers={getCustomers} people={people} getPeople={getPeople} getSales={getSales}/>} />
          </Route>
          <Route path="people">
              <Route path="" element={<PeopleList people={people} />} />
              <Route path="new" element={<PersonForm getPeople={getPeople} getVehicleModels={getVehicleModels} />} />
          </Route>
          <Route path="customers">
              <Route path="" element={<CustomersList customers={customers} />} />
              <Route path="new" element={<CustomerForm getCustomers={getCustomers} getVehicleModels={getVehicleModels} />} />
          </Route>
          <Route path="records">
              <Route path="" element={<RecordsList records={records} getRecords={getRecords}/>} />
              <Route path="new" element={<RecordForm getRecords={getRecords} getVehicleModels={getVehicleModels} />} />
          </Route>
          </Routes>
        </div>
      </BrowserRouter>
    );
}
export default App
