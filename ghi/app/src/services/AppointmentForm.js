import React, {useState} from "react";

function AppointmentForm(props){
    const [vin, setVin] = useState('')
    const [customer, setCustomer] = useState("")
    const [date, setDate] = useState("")
    const [time, setTime] = useState("")
    const [technician, setTechnician] = useState("")
    const [reason_for_service, setReason] = useState("")

    const handleVinChange = (event) =>{
        const value = event.target.value
        setVin(value)
    }
    const handleCustomerChange = (event) =>{
        const value = event.target.value
        setCustomer(value)
    }
    const handleDateChange = (event) =>{
        const value = event.target.value
        setDate(value)
    }
    const handleTimeChange = (event) =>{
        const value = event.target.value
        setTime(value)
    }
    const handleTechnicianChange = (event) =>{
        const value = event.target.value
        setTechnician(value)
    }
    const handleReasonChange = (event) =>{
        const value = event.target.value
        setReason(value)
    }

    const handleSubmit = async (event) =>{
        event.preventDefault()
        const data = {}
        data.vin = vin
        data.customer = customer
        data.date = date
        data.time = time
        data.technician = technician
        data.reason_for_service = reason_for_service

        const appointmentUrl = "http://localhost:8080/api/appointments/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(appointmentUrl, fetchConfig)
        if (response.ok){
            const newAppointment = await response.json()
            setVin("")
            setCustomer("")
            setDate("")
            setTime("")
            setTechnician("")
            setReason("")
            props.getAppointments()
        }
    }
    return (
        <div className="container">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Make an appointment</h1>
          <form onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
              <input onChange={handleVinChange} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" value={vin} />
              <label htmlFor="vin">VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleCustomerChange} placeholder="customer" required type="text" name="customer" id="customer" className="form-control" value={customer} />
              <label htmlFor="customer">Customer Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleDateChange} placeholder="date" type="date" name="date" id="date" className="form-control" value={date} />
              <label htmlFor="date">Appointment Date</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleTimeChange} placeholder="time" type="time" name="time" id="time" className="form-control" value={time} />
              <label htmlFor="appt">Appointment Time</label>
            </div>
            <div className="mb-3">
              <select onChange={handleTechnicianChange} required name="technician" id="technician" className="form-select">
                <option value="">Choose a technician</option>
                {props.technicians.map(technician => {
                      return (
                          <option key={technician.id} value={technician.id}>
                              {technician.name} - {technician.employee_num}
                          </option>
                      )
                    })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleReasonChange} placeholder="reason" required type="text" name="reason_for_service" id="reason_for_service" className="form-control" value={reason_for_service} />
              <label htmlFor="reason_for_service">Reason for Service</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
    );

}
export default AppointmentForm;
