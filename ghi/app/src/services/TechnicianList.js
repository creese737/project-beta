import React from 'react';

function TechnicianList(props) {
    return (
        <div>
            <h2 className="mt-5"><b>Technicians</b></h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Employee Number</th>
                    </tr>
                </thead>
                <tbody>
                    {props.technicians.map(technician => {
                        return (
                            <tr key={ technician.id }>
                                <td>{ technician.name }</td>
                                <td>{ technician.employee_number}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}
export default TechnicianList;