import { useState } from "react";

function ServiceHistory(props){
    const [vin, vinSearch] = useState("")

    const handleVin = async (event) => {
        const value = event.target.value
        vinSearch(value)
    }
    const handleSearch = async (event) => {
        event.preventDefault();
        const filteredVins = props.appointments.filter((appointment) => appointment.vin.includes(vin))
        props.getVinAppointments()
    }
    return (
        <>
        <div className="input-group">
            <input onChange={handleSearch} type="search" className="form-control rounded" placeholder="Search" aria-label="Search" aria-describedby="search-addon" />
            <button onClick={ () => handleVin} type="button" className="btn btn-success">Search Vin</button>
        </div>
        <table className="table table-striped">
        <thead>
            <tr>
            <th>Vin</th>
            <th>Customer name</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason for Service</th>
            <th></th>
            </tr>
        </thead>
        <tbody>
        {props.appointments.filter(appointments => appointments.finished === true).map(appointments => {
            return (
              <tr key={appointments.id}>
                <td>{appointments.vin}</td>
                <td>{appointments.customer}</td>
                <td>{ new Date(appointments.date).toLocaleDateString("en-US") }</td>
                <td>{ new Date(appointments.time).toLocaleTimeString([], { hour: "2-digit", minute: "2-digit"}) }</td>
                <td>{appointments.technician.name}</td>
                <td>{appointments.reason}</td>
              </tr>
            );
          })}
        </tbody>
        </table>
        </>
    )

}
export default ServiceHistory;
