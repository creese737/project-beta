function AppointmentList(props){
    const cancelAppointment = async (appointment) => {
        const appointmentUrl = `http://localhost:8080/api/appointments/${appointment.id}/`
        const fetchConfig = {method: "delete"}
        const response = await fetch(appointmentUrl, fetchConfig)
        if (response.ok){
            props.getAppointments()
        }
    }
    const finishAppointment = async (appointment) => {
        const appointmentUrl = `http://localhost:8080/api/appointments/${appointment.id}/`
        const fetchConfig = {
            method: "put",
            body: JSON.stringify({finished: true}),
            headers: {
                "Content-Type": "application/json",
            }
        }
        const response = await fetch(appointmentUrl, fetchConfig)
        if (response.ok){
            props.getAppointments()
        }
    }

    return (
        <>
            <div className="container">
                <div className="input-group">
                </div>
            </div>
            <div>
                <h1>Service Appointments</h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Customer name</th>
                            <th>Vip</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Technician</th>
                            <th>Reason for Service</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    {props.appointments.map(appointment => {
                        if  (appointment.finished === false)
                        return (
                            <tr key={appointment.id}>
                                <td>{appointment.vin}</td>
                                <td>{appointment.customer}</td>
                                <td>{ new Date(appointment.date).toLocaleDateString("en-US") }</td>
                                <td>{ new Date(appointment.time).toLocaleTimeString([], { hour: "2-digit", minute: "2-digit"}) }</td>
                                <td>{appointment.technician}</td>
                                <td>{appointment.reason}</td>
                                <td>
                                    <button
                                        id={ appointment.id } onClick={() => cancelAppointment(appointment)}
                                        type="button" className="btn btn-danger">Cancel
                                    </button>
                                    <button
                                        id={ appointment.id } onClick={() => finishAppointment(appointment)}
                                        type="button" className="btn btn-success"> Finished
                                    </button>
                                </td>
                            </tr>
                        );
                    })}
                    </tbody>
                </table>
            </div>
        </>
    )

}
export default AppointmentList;
