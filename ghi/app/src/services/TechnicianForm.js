import React, {useState} from "react";

function TechnicianForm(props){
    const [name, setName] = useState("")
    const [employee_num, setEmployeeNum] = useState("")

    const handleNameChange = (event) =>{
        const value = event.target.value
        setName(value)
    }
    const handleEmployeeNumChange = (event) =>{
        const value = event.target.value
        setEmployeeNum(value)
    }
    const handleSubmit = async (event) =>{
        event.preventDefault()
        const data = {}
        data.name = name
        data.employee_num = employee_num
        const technicianUrl = 'http://localhost:8080/api/technicians/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json()",
            },
        }
        const response = await fetch(technicianUrl, fetchConfig)
        if (response.ok){
            const newTechnician = await response.json()
            setName("")
            setEmployeeNum("")
            props.getTechnicians()
        }
    }


    return(
        <div className="container">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Enter a technician</h1>
                    <form onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} placeholder="name" required type="text" name="name" id="name" className="form-control" value={name} />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEmployeeNumChange} placeholder="employee_num" required type="number" name="employee_num" id="employee_num" className="form-control" value={employee_num} />
                            <label htmlFor="employee_num">Employee Number</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );

}
export default TechnicianForm;
