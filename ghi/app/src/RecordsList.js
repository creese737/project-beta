function RecordsList({records, getRecords}) {
    const deleteRecord = async (record) => {
        const recordUrl = `http://localhost:8090/api/records/${record.id}/`
        const fetchConfig = {method: "delete"}
        const response = await fetch(recordUrl, fetchConfig)
        if (response.ok) {
            getRecords()
        }
    }
    if (records === undefined) {
        return null;
    }

    return (
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Automobile</th>
                <th>Sales Person</th>
                <th>Customer</th>
                <th>Color</th>
                <th>Year</th>
                <th>VIN</th>
            </tr>
            </thead>
            <tbody>
            {records.map(record => {
                return (
                <tr key={record.id}>
                    <td>{ record.automobile.vin }</td>
                    <td>{ record.person.name }</td>
                    <td>{ record.customer.name }</td>
                    <td>{ record.color }</td>
                    <td>{ record.year }</td>
                    <td>{ record.vin }</td>
                    <td>
                        <button type="button" className="btn btn-danger" onClick={() => deleteRecord(record)}>Delete</button>
                    </td>
                </tr>
                );
            })}
            </tbody>
        </table>
    );
}
export default RecordsList;
