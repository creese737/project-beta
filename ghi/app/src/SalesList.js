function SalesList(props) {
    const deleteSale = async (sale) => {
        const saleUrl = `http://localhost:8090/api/sales/${sale.id}/`
        const fetchConfig = {method: "delete"}
        const response = await fetch(saleUrl, fetchConfig)
        if (response.ok) {
            props.getSales()
        }
    }

    if (props.sales === undefined) {
        return null;
    }

    return (
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Sales Person Name</th>
                <th>Sales Person Employee Number</th>
                <th>Purchaser Name</th>
                <th>Automobile VIN</th>
                <th>Sale Price</th>
            </tr>
            </thead>
            <tbody>
            {props.sales.map(sale => {
                return (
                <tr key={sale.id}>
                    <td>{ sale.person.name }</td>
                    <td>{ sale.person.employee_number }</td>
                    <td>{ sale.customer.name }</td>
                    <td>{ sale.vin }</td>
                    <td>{ sale.price }</td>
                    <td>
                        <button type="button" className="btn btn-danger" onClick={() => deleteSale(sale)}>Delete</button>
                    </td>
                </tr>
                );
            })}
            </tbody>
        </table>
    );
}
export default SalesList;
