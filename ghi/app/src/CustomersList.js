function CustomersList(props) {
    const deleteCustomer = async (customer) => {
        const customersUrl = `http://localhost:8090/api/customers/${customer.id}/`
        const fetchConfig = {method: "delete"}
        const response = await fetch(customersUrl, fetchConfig)
        if (response.ok) {
            props.getCustomers()
        }
    }

    if (props.customers === undefined) {
        return null;
    }

    return (
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Address</th>
                <th>Phone Number</th>
            </tr>
            </thead>
            <tbody>
            {props.customers.map(customer => {
                return (
                <tr key={customer.id}>
                    <td>{ customer.name }</td>
                    <td>{ customer.address }</td>
                    <td>{ customer.phone.number }</td>
                    <td>
                        <button type="button" className="btn btn-danger" onClick={() => deleteCustomer(customer)}>Delete</button>
                    </td>
                </tr>
                );
            })}
            </tbody>
        </table>
    );
}
export default CustomersList;
