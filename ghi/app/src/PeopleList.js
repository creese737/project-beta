function PeopleList(props) {
    const deletePerson = async (person) => {
        const peopleUrl = `http://localhost:8090/api/people/${person.id}/`
        const fetchConfig = {method: "delete"}
        const response = await fetch(peopleUrl, fetchConfig)
        if (response.ok) {
            props.getPeople()
        }
    }

    if (props.people === undefined) {
        return null;
    }

    return (
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Employee Number</th>
            </tr>
            </thead>
            <tbody>
            {props.people.map(person => {
                return (
                <tr key={person.id}>
                    <td>{ person.name }</td>
                    <td>{ person.employee_number }</td>
                    <td>
                        <button type="button" className="btn btn-danger" onClick={() => deletePerson(person)}>Delete</button>
                    </td>
                </tr>
                );
            })}
            </tbody>
        </table>
    );
}
export default PeopleList;
