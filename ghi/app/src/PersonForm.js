import React, { useEffect , useState } from 'react';

function PersonForm(props){
    const [name, setName] = useState('')
    const [employee_number, setEmployeeNumber] = useState('')

    const handleName = (event) => {
        const value = event.target.value
        setName(value)
    }
    const handleEmployeeNumber = (event) => {
        const value = event.target.value
        setEmployeeNumber(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}
        data.name = name
        data.employee_number = employee_number

        const peopleUrl='http://localhost:8090/api/people/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const response = await fetch(peopleUrl, fetchConfig);
        if (response.ok) {
            const newPerson = await response.json()
            setName('')
            setEmployeeNumber('')

            props.getPeople()
        }
    }
    return (
        <div className="container">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Add a sales person</h1>
                <form onSubmit={handleSubmit} id="create-person-form">
                    <div className="form-floating mb-3">
                    <input onChange={handleName} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                    <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input onChange={handleEmployeeNumber} value={employee_number} placeholder="Employee Number" required type="text" name="employee_number" id="employee_number" className="form-control"/>
                    <label htmlFor="employee_number">Employee Number</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
                </div>
            </div>
        </div>
        );
}
export default PersonForm;
