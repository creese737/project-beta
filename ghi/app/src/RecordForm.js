import React, { useEffect , useState } from 'react';

function RecordForm(props){
    // const [automobiles, setAutomobiles] = useState([])
    const [automobile, setAutomobile] = useState('')
    const [person, setPerson] = useState('')
    const [customer, setCustomer] = useState('')
    const [price, setPrice] = useState('')

    const handleAutomobile = (event) => {
        const value = event.target.value
        setAutomobile(value)
    }
    const handlePerson = (event) => {
        const value = event.target.value
        setPerson(value)
    }
    const handleCustomer = (event) => {
        const value = event.target.value
        setCustomer(value)
    }
    const handlePrice = (event) => {
        const value = event.target.value
        setPrice(value)
    }


    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}
        data.automobile = automobile
        data.person = person
        data.customer = customer
        data.price = price

        const recordUrl='http://localhost:8090/api/records/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const response = await fetch(recordUrl, fetchConfig);
        if (response.ok) {
            const newRecord = await response.json()
            setAutomobile('')
            setPerson('')
            setCustomer('')
            setPrice('')

            props.getRecord()
        }
    }

     return (
        <div className="container">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Create a sale record</h1>
                <form onSubmit={handleSubmit} id="create-record-form">
                    <div className="form-floating mb-3">
                    <input onChange={handleAutomobile} value={automobile} placeholder="Automobile" required type="text" name="automobile" id="automobile" className="form-control"/>
                    <label htmlFor="automobile">Automobile</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input onChange={handlePerson} value={person} placeholder="Person" required type="text" name="person" id="person" className="form-control"/>
                    <label htmlFor="person">Sales Person</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input onChange={handleCustomer} value={customer} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control"/>
                    <label htmlFor="customer">Customer</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input onChange={handlePrice} value={price} placeholder="Price" required type="text" name="price" id="price" className="form-control"/>
                    <label htmlFor="price">Price</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
                </div>
            </div>
        </div>
        );
}
export default RecordForm
